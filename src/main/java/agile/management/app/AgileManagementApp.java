package agile.management.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by Marcin Ławicki (mak)
 */
@SpringBootApplication
public class AgileManagementApp {

	public static void main(String[] args) {
		SpringApplication.run(AgileManagementApp.class, args);
	}
}
