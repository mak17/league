package agile.management.app.Services;

import agile.management.app.model.Club;
import agile.management.app.model.Match;
import agile.management.app.repositories.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Service
public class MatchService {

    @Autowired
    ClubRepository clubRepository;

    public List<Match> playMatches() {

        List<Match> results = new ArrayList<>();

        Iterator<Club> clubIter = clubRepository.findAll().iterator();
        List<Club> clubList = new ArrayList<Club>();
        while (clubIter.hasNext())
            clubList.add(clubIter.next());

        clubList.sort(Comparator.comparing(Club::getScore).reversed());

        int id = 0;
        int max = 10;

        while(id<5){

            Club club = clubList.get(id);
            Integer scoreClub = getGoal();
            id++;

            Club club2 = clubList.get(max-id);
            Integer scoreClub2 = getGoal();

            Match pair = new Match(club, club2, scoreClub, scoreClub2);
            results.add(pair);
        }
        addScore(results);
        return results;
    }

    public void addScore(List<Match> results) {
        Integer scoreForClubOne = 0;
        Integer scoreForClubTwo = 0;

        for(Match pair : results) {
            if(pair.getScoreClubOne() > pair.getScoreClubTwo()) {
                scoreForClubOne = 3;
                scoreForClubTwo = 0;
            }  else if (pair.getScoreClubOne() < pair.getScoreClubTwo()) {
                scoreForClubOne = 0;
                scoreForClubTwo = 3;
            } else {
                scoreForClubOne = 1;
                scoreForClubTwo = 1;
            }

            Club clubOne = clubRepository.findById(pair.getClubOne().getId()).get();
            clubOne.setScore(clubOne.getScore()+scoreForClubOne);
            clubRepository.save(clubOne);

            Club clubTwo = clubRepository.findById(pair.getClubTwo().getId()).get();
            clubTwo.setScore(clubTwo.getScore()+scoreForClubTwo);
            clubRepository.save(clubTwo);
        }
    }

    public Integer getGoal() {
        Random random = new Random();
        return random.nextInt((6 - 0) + 1) + 0;
    }
}
