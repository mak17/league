package agile.management.app.Services;

import agile.management.app.model.Coach;
import agile.management.app.repositories.CoachRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Marcin Ławicki (mak)
 */
@Service
public class CoachService {

    @Autowired
    CoachRepository coachRepository;

    public Set<Coach> getOnlyFreeChoaches() {
        Set<Coach> freeCoaches = new HashSet<Coach>();

        for ( Coach coach : coachRepository.findAll()) {
            if (coach.getClub() == null) {
                freeCoaches.add(coach);
            }
        }
        return freeCoaches;
    }
}
