package agile.management.app.Services;

import agile.management.app.enums.Position;
import agile.management.app.model.Player;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Marcin Ławicki (mak)
 */
@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    public Set<Player> getOnlyFreePlayers() {
        Set<Player> freePlayers = new HashSet<Player>();

        for ( Player player : playerRepository.findAll()) {
            if (player.getClub() == null) {
                freePlayers.add(player);
            }
        }
        return freePlayers;
    }

    public List<Player> filterPlayers(Position position) {
        List<Player> players = new ArrayList<>();

        for( Player player : playerRepository.findAll()) {
            if(player.getPosition() == position) {
                players.add(player);
            }
        }
        return  players;
    }
}
