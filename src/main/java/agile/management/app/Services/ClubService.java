package agile.management.app.Services;

import agile.management.app.model.Club;
import agile.management.app.repositories.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Service
public class ClubService {

    @Autowired
    ClubRepository clubRepository;

    public List<Club> sortByScore() {
        List<Club> clubs = new ArrayList<>();
        for(Club club : clubRepository.findAll() ) {
            clubs.add(club);
        }
        clubs.sort(Comparator.comparing(Club::getScore).reversed());
        return clubs;
    }
}
