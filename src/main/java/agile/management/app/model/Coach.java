package agile.management.app.model;

import javax.persistence.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Entity
@Table(name = "coach")
public class Coach {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String surname;

    private String country;

    private Integer age;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "coach_id", nullable = true)
    private Club club;

    public Coach () {}

    public Coach(String name, String surname, String country, Integer age, Club club) {
        this.name = name;
        this.surname = surname;
        this.country = country;
        this.age = age;
        this.club = club;
    }

    public Coach(String name, String surname, String country, Integer age) {
        this.name = name;
        this.surname = surname;
        this.country = country;
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public String getClubName() {
        try {
            return this.club.getName();
        } catch (Exception e) {
            return "No club";
        }
    }
}
