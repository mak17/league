package agile.management.app.model;

/**
 * Created by Marcin Ławicki (mak)
 */
public class Match {

    private Club clubOne;

    private Club clubTwo;

    private Integer scoreClubOne;

    private Integer scoreClubTwo;

    public Match(Club clubOne, Club clubTwo, Integer scoreClubOne, Integer scoreClubTwo) {
        this.clubOne = clubOne;
        this.clubTwo = clubTwo;
        this.scoreClubOne = scoreClubOne;
        this.scoreClubTwo = scoreClubTwo;
    }

    public Club getClubOne() {
        return clubOne;
    }

    public void setClubOne(Club clubOne) {
        this.clubOne = clubOne;
    }

    public Club getClubTwo() {
        return clubTwo;
    }

    public void setClubTwo(Club clubTwo) {
        this.clubTwo = clubTwo;
    }

    public Integer getScoreClubOne() {
        return scoreClubOne;
    }

    public void setScoreClubOne(Integer scoreClubOne) {
        this.scoreClubOne = scoreClubOne;
    }

    public Integer getScoreClubTwo() {
        return scoreClubTwo;
    }

    public void setScoreClubTwo(Integer scoreClubTwo) {
        this.scoreClubTwo = scoreClubTwo;
    }
}
