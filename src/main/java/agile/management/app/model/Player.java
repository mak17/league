package agile.management.app.model;

import agile.management.app.enums.Position;
import javax.persistence.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Entity
@Table(name = "player")
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String surname;

    private Position position;

    private Integer age;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "club_id", nullable = true)
    private Club club;

    public Player() {}

    public Player ( String name, String surname, Position position, Integer age) {
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.age = age;
    }

    public Player ( String name, String surname, Position position, Integer age, Club club) {
        this.name = name;
        this.surname = surname;
        this.position = position;
        this.age = age;
        this.club = club;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Club getClub() {
        return club;
    }

    public void setClub(Club club) {
        this.club = club;
    }

    public String getClubName() {
        try {
            return this.club.getName();
        } catch (Exception e) {
            return "No club";
        }
    }

    public boolean hasClub() {
        if ( this.club == null) {
            return false;
        }
        return true;
    }
}
