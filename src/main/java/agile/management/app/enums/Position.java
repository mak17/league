package agile.management.app.enums;

/**
 * Created by Marcin Ławicki (mak)
 */
public enum Position {
    GOALKEEPER,
    DEFENDER,
    STRIKER,
}
