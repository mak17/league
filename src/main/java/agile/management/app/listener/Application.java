package agile.management.app.listener;

import agile.management.app.enums.Position;
import agile.management.app.model.*;
import agile.management.app.repositories.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * Created by Marcin Ławicki (mak)
 */
@Component
public class Application implements ApplicationListener<ContextRefreshedEvent> {

    private ClubRepository clubRepository;
    private PlayerRepository playerRepository;
    private CoachRepository coachRepository;

    public Application(ClubRepository clubRepository, PlayerRepository playerRepository, CoachRepository coachRepository) {
        this.clubRepository = clubRepository;
        this.playerRepository = playerRepository;
        this.coachRepository = coachRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        initData();
    }

    private void initData(){

        Coach coach1 = new Coach("Name 1", "Coach 1", "Country 1", 56);
        Coach coach2 = new Coach("Name 2", "Coach 2", "Country 2", 76);
        Coach coach3 = new Coach("Name 3", "Coach 3", "Country 3", 45);
        Coach coach4 = new Coach("Name 4", "Coach 4", "Country 4", 87);
        Coach coach5 = new Coach("Name 5", "Coach 5", "Country 5", 90);
        Coach coach6 = new Coach("Name 6", "Coach 6", "Country 6", 78);
        Coach coach7 = new Coach("Name 7", "Coach 7", "Country 7", 54);
        Coach coach8 = new Coach("Name 8", "Coach 8", "Country 8", 56);
        Coach coach9 = new Coach("Name 9", "Coach 9", "Country 8", 56);
        Coach coach10 = new Coach("Name 10", "Coach 10", "Country 10", 67);
        Coach coach11 = new Coach("Name 11", "Coach 11", "Country 11", 76);
        Coach coach12 = new Coach("Name 12", "Coach 11", "Country 12", 78);
        coachRepository.save(coach11);
        coachRepository.save(coach12);

        Club club1 = new Club("Club 1",3, coach1);
        Club club2 = new Club("Club 2",12, coach2);
        Club club3 = new Club("Club 3",14, coach3);
        Club club4 = new Club("Club 4",15, coach4);
        Club club5 = new Club("Club 5",5, coach5);
        Club club6 = new Club("Club 6",6, coach6);
        Club club7 = new Club("Club 7",14, coach7);
        Club club8 = new Club("Club 8",3, coach8);
        Club club9 = new Club("Club 9",0, coach9);
        Club club10 = new Club("Club 10",4, coach10);

        coach1.setClub(club1);
        coach2.setClub(club2);
        coach3.setClub(club3);
        coach4.setClub(club4);
        coach5.setClub(club5);
        coach6.setClub(club6);
        coach7.setClub(club7);
        coach8.setClub(club8);
        coach9.setClub(club9);
        coach10.setClub(club10);

        Player player1 = new Player("Name 1", "Surname 1", Position.GOALKEEPER, 23, club1);
        player1.setClub(club1);

        Player player2 = new Player("Name 2", "Surname 2", Position.DEFENDER, 18);
        player2.setClub(club1);

        Player player3 = new Player("Name 3", "Surname 3", Position.STRIKER, 23);
        player3.setClub(club1);

        Player player4 = new Player("Name 4", "Surname 4", Position.GOALKEEPER, 25);
        player4.setClub(club2);

        Player player5 = new Player("Name 5", "Surname 5", Position.DEFENDER, 24);
        player5.setClub(club2);

        Player player6 = new Player("Name 6", "Surname 6", Position.STRIKER, 30);
        player6.setClub(club2);

        Player player7 = new Player("Name 7", "Surname 7", Position.GOALKEEPER, 27);
        player7.setClub(club3);

        Player player8 = new Player("Name 8", "Surname 8", Position.DEFENDER, 25);
        player8.setClub(club3);

        Player player9 = new Player("Name 9", "Surname 9", Position.STRIKER, 23);
        player9.setClub(club3);

        Player player10 = new Player("Name 10", "Surname 10", Position.GOALKEEPER, 23);
        player10.setClub(club4);

        Player player11 = new Player("Name 11", "Surname 11", Position.DEFENDER, 26);
        player11.setClub(club4);

        Player player12 = new Player("Name 12", "Surname 12", Position.STRIKER, 27);
        player12.setClub(club4);

        Player player13 = new Player("Name 13", "Surname 13", Position.GOALKEEPER, 22);
        player13.setClub(club5);

        Player player14 = new Player("Name 14", "Surname 14", Position.DEFENDER, 21);
        player14.setClub(club5);

        Player player15 = new Player("Name 15", "Surname 15", Position.STRIKER, 25);
        player15.setClub(club5);

        Player player16 = new Player("Name 16", "Surname 16", Position.GOALKEEPER, 28);
        player16.setClub(club6);

        Player player17 = new Player("Name 17", "Surname 17", Position.DEFENDER, 29);
        player17.setClub(club6);

        Player player18 = new Player("Name 18", "Surname 18", Position.STRIKER, 23);
        player18.setClub(club6);

        Player player19 = new Player("Name 19", "Surname 19", Position.GOALKEEPER, 23);
        player19.setClub(club7);

        Player player20 = new Player("Name 20", "Surname 20", Position.DEFENDER, 22);
        player20.setClub(club7);

        Player player21 = new Player("Name 21", "Surname 21", Position.STRIKER, 21);
        player21.setClub(club7);

        Player player22 = new Player("Name 22", "Surname 22", Position.GOALKEEPER, 21);
        player22.setClub(club8);

        Player player23 = new Player("Name 23", "Surname 23", Position.DEFENDER, 35);
        player23.setClub(club8);

        Player player24 = new Player("Name 24", "Surname 24", Position.STRIKER, 23);
        player24.setClub(club8);

        Player player25 = new Player("Name 25", "Surname 25", Position.GOALKEEPER, 23);
        player25.setClub(club9);

        Player player26 = new Player("Name 26", "Surname 26", Position.DEFENDER, 22);
        player26.setClub(club9);

        Player player27 = new Player("Name 27", "Surname 27", Position.STRIKER, 25);
        player27.setClub(club9);

        Player player28 = new Player("Name 28", "Surname 18", Position.GOALKEEPER, 21);
        player28.setClub(club10);

        Player player29 = new Player("Name 29", "Surname 29", Position.DEFENDER, 20);
        player29.setClub(club10);

        Player player30 = new Player("Name 30", "Surname 30", Position.STRIKER, 23);
        player30.setClub(club10);

        Player player31 = new Player("Name 31", "Surname 31", Position.GOALKEEPER, 24);
        playerRepository.save(player31);

        Player player32 = new Player("Name 32", "Surname 32", Position.DEFENDER, 21);
        playerRepository.save(player32);

        Player player33 = new Player("Name 33", "Surname 33", Position.STRIKER, 20);
        playerRepository.save(player33);

        Player player34 = new Player("Name 34", "Surname 34", Position.GOALKEEPER, 19);
        playerRepository.save(player34);

        Player player35 = new Player("Name 35", "Surname 35", Position.DEFENDER, 19);
        playerRepository.save(player35);

        Player player36 = new Player("Name 36", "Surname 36", Position.STRIKER, 24);
        playerRepository.save(player36);

        Player player37 = new Player("Name 37", "Surname 37", Position.GOALKEEPER, 23);
        playerRepository.save(player37);

        Player player38 = new Player("Name 38", "Surname 38", Position.DEFENDER, 26);
        playerRepository.save(player38);

        Player player39 = new Player("Name 39", "Surname 39", Position.STRIKER, 23);
        playerRepository.save(player39);

        Player player40 = new Player("Name 40", "Surname 40", Position.DEFENDER, 23);
        playerRepository.save(player40);

        club1.getPlayers().add(player1);
        club1.getPlayers().add(player2);
        club1.getPlayers().add(player3);
        club2.getPlayers().add(player4);
        club2.getPlayers().add(player5);
        club2.getPlayers().add(player6);
        club3.getPlayers().add(player7);
        club3.getPlayers().add(player8);
        club3.getPlayers().add(player9);
        club4.getPlayers().add(player10);
        club4.getPlayers().add(player11);
        club4.getPlayers().add(player12);
        club5.getPlayers().add(player13);
        club5.getPlayers().add(player14);
        club5.getPlayers().add(player15);
        club6.getPlayers().add(player16);
        club6.getPlayers().add(player17);
        club6.getPlayers().add(player18);
        club7.getPlayers().add(player19);
        club7.getPlayers().add(player20);
        club7.getPlayers().add(player21);
        club8.getPlayers().add(player22);
        club8.getPlayers().add(player23);
        club8.getPlayers().add(player24);
        club9.getPlayers().add(player25);
        club9.getPlayers().add(player26);
        club9.getPlayers().add(player27);
        club10.getPlayers().add(player28);
        club10.getPlayers().add(player29);
        club10.getPlayers().add(player30);

        clubRepository.save(club1);
        clubRepository.save(club2);
        clubRepository.save(club3);
        clubRepository.save(club4);
        clubRepository.save(club5);
        clubRepository.save(club6);
        clubRepository.save(club7);
        clubRepository.save(club8);
        clubRepository.save(club9);
        clubRepository.save(club10);
    }
}
