package agile.management.app.repositories;

import agile.management.app.model.Club;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Marcin Ławicki (mak)
 */
@Repository
public interface ClubRepository extends CrudRepository<Club, Long> {
}
