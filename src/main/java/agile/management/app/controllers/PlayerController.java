package agile.management.app.controllers;

import agile.management.app.Services.PlayerService;
import agile.management.app.enums.Position;
import agile.management.app.repositories.ClubRepository;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class PlayerController {

    private ClubRepository clubRepository;
    private PlayerRepository playerRepository;

    @Autowired
    PlayerService playerService;

    public PlayerController(ClubRepository clubRepository, PlayerRepository playerRepository) {
        this.clubRepository = clubRepository;
        this.playerRepository = playerRepository;
    }

    @RequestMapping (value="playerDetails/{idPlayer}", method = RequestMethod.GET)
    public String showDetails(@PathVariable Long idPlayer, Model model) {
        model.addAttribute("player", playerRepository.findById(idPlayer).get());
        return "playerDetails";
    }

    @PostMapping("/filterPlayer")
    public String saveDetails(@RequestParam("position") Position position, Model model) {
        model.addAttribute("players", playerService.filterPlayers(position));
        return "players";
    }

    @RequestMapping("resetFilter")
    public String resetFilter(Model model) {
        model.addAttribute("players", playerRepository.findAll());
        return "players";
    }

    @RequestMapping (value="formPlayer")
    public String formPlayer() {
        return "createPlayer";
    }
}
