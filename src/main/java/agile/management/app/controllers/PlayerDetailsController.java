package agile.management.app.controllers;

import agile.management.app.model.Club;
import agile.management.app.model.Player;
import agile.management.app.repositories.ClubRepository;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class PlayerDetailsController {

    private ClubRepository clubRepository;
    private PlayerRepository playerRepository;

    public PlayerDetailsController(ClubRepository clubRepository, PlayerRepository playerRepository) {
        this.clubRepository = clubRepository;
        this.playerRepository = playerRepository;
    }

    @RequestMapping (value="leaveClub/{idPlayer}", method = RequestMethod.GET)
    public String leaveClub(@PathVariable Long idPlayer, Model model) {

        Player player = playerRepository.findById(idPlayer).get();
        Club club = clubRepository.findById(player.getClub().getId()).get();

        club.getPlayers().remove(player);
        clubRepository.save(club);

        player.setClub(null);
        playerRepository.save(player);

        model.addAttribute("players", playerRepository.findAll());
        return "players";
    }

    @RequestMapping (value="showClubs/{idPlayer}", method = RequestMethod.GET)
    public String showClubs(@PathVariable Long idPlayer, Model model) {

        model.addAttribute("player", playerRepository.findById(idPlayer).get());
        model.addAttribute("clubs", clubRepository.findAll());
        return "clublist";
    }

    @RequestMapping (value="endCareer/{idPlayer}", method = RequestMethod.GET)
    public String endCareer(@PathVariable Long idPlayer, Model model) {
        Player player = playerRepository.findById(idPlayer).get();

        if (player.getClub() != null) {
            Club club = clubRepository.findById(player.getClub().getId()).get();
            club.getPlayers().remove(player);
            clubRepository.save(club);
        }

        playerRepository.delete(player);

        model.addAttribute("players", playerRepository.findAll());
        return "players";
    }

    @RequestMapping (value="editForm/{idPlayer}", method = RequestMethod.GET)
    public String editForm(@PathVariable Long idPlayer, Model model) {
        model.addAttribute("player", playerRepository.findById(idPlayer).get());
        return "editPlayer";
    }
}
