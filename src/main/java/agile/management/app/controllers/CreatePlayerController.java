package agile.management.app.controllers;

import agile.management.app.enums.Position;
import agile.management.app.model.Player;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class CreatePlayerController {

    private PlayerRepository playerRepository;

    public CreatePlayerController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @PostMapping("/createPlayer")
    public String saveDetails(@RequestParam("name") String name,
                              @RequestParam("surname") String surname,
                              @RequestParam("position") Position position,
                              @RequestParam("age") String age,
                              Model model) {

        Player player = new Player(name, surname, position, Integer.parseInt(age));
        playerRepository.save(player);
        model.addAttribute("players", playerRepository.findAll());
        return "players";
    }
}
