package agile.management.app.controllers;

import agile.management.app.Services.MatchService;
import agile.management.app.repositories.ClubRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class leagueController {

    private ClubRepository clubRepository;

    @Autowired
    MatchService matchService;
    public leagueController(ClubRepository clubRepository) {
        this.clubRepository = clubRepository;
    }

    @RequestMapping (value="club/{id}", method = RequestMethod.GET)
    public String getClub(@PathVariable Long id, Model model){
        model.addAttribute("club", clubRepository.findById(id).get());
        return "club";
    }

    @RequestMapping("playMath")
    public String playMath(Model model) {
        model.addAttribute("pairs", matchService.playMatches());
        return "resultsMatches";
    }
}
