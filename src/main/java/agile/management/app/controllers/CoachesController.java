package agile.management.app.controllers;

import agile.management.app.repositories.CoachRepository;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class CoachesController {

    private CoachRepository coachRepository;

    public CoachesController(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @RequestMapping (value="formCoach")
    public String formCoach() {
        return "createCoach";
    }
}
