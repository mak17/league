package agile.management.app.controllers;

import agile.management.app.Services.ClubService;
import agile.management.app.repositories.ClubRepository;
import agile.management.app.repositories.CoachRepository;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class IndexController {

    private ClubRepository clubRepository;
    private PlayerRepository playerRepository;
    private CoachRepository coachRepository;

    @Autowired
    ClubService clubService;

    public IndexController(ClubRepository clubRepository, PlayerRepository playerRepository, CoachRepository coachRepository) {
        this.clubRepository = clubRepository;
        this.playerRepository = playerRepository;
        this.coachRepository = coachRepository;
    }

    @RequestMapping("/")
    public String getIndex() {
        return "index";
    }

    @RequestMapping("league")
    public String getLeague(Model model) {

        model.addAttribute("clubs", clubService.sortByScore());
        return "league";
    }

    @RequestMapping("players")
    public String getPlayers(Model model) {
        model.addAttribute("players", playerRepository.findAll());
        return "players";
    }

    @RequestMapping("coaches")
    public String getCoaches(Model model) {
        model.addAttribute("coaches", coachRepository.findAll());
        return "coaches";
    }
}
