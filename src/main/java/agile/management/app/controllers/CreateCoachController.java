package agile.management.app.controllers;

import agile.management.app.model.Coach;
import agile.management.app.repositories.CoachRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class CreateCoachController {

    private CoachRepository coachRepository;

    public CreateCoachController(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @PostMapping("/createCoach")
    public String saveDetails(@RequestParam("name") String name,
                              @RequestParam("surname") String surname,
                              @RequestParam("country") String country,
                              @RequestParam("age") String age,
                              Model model) {

        Coach coach = new Coach(name, surname, country, Integer.parseInt(age));
        coachRepository.save(coach);
        model.addAttribute("coaches", coachRepository.findAll());
        return "coaches";
    }
}
