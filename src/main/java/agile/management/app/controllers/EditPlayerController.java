package agile.management.app.controllers;

import agile.management.app.model.Player;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class EditPlayerController {

    private PlayerRepository playerRepository;

    public EditPlayerController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @PostMapping("/editPlayer/{updatePlayerId}")
    public String updatePlayer(@PathVariable Long updatePlayerId, @ModelAttribute Player updatePlayer, Model model) {
        Player player = playerRepository.findById(updatePlayerId).get();
        player.setName(updatePlayer.getName());
        player.setSurname(updatePlayer.getSurname());
        player.setAge(updatePlayer.getAge());
        player.setPosition(updatePlayer.getPosition());
        playerRepository.save(player);
        model.addAttribute("players", playerRepository.findAll());
        return "players";
    }
}
