package agile.management.app.controllers;

import agile.management.app.Services.PlayerService;
import agile.management.app.repositories.CoachRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class CoachController {

    private CoachRepository coachRepository;

    @Autowired
    PlayerService playerService;

    public CoachController(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @RequestMapping (value="coachDetails/{idCoach}", method = RequestMethod.GET)
    public String coachDetails(@PathVariable Long idCoach, Model model) {
        model.addAttribute("coach", coachRepository.findById(idCoach).get());
        return "coachDetails";
    }
}
