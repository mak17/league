package agile.management.app.controllers;

import agile.management.app.model.Club;
import agile.management.app.model.Player;
import agile.management.app.repositories.ClubRepository;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class FreePlayersController {

    private PlayerRepository playerRepository;
    private ClubRepository clubRepository;

    public FreePlayersController(ClubRepository clubRepository, PlayerRepository playerRepository) {
        this.clubRepository = clubRepository;
        this.playerRepository = playerRepository;
    }

    @RequestMapping (value="addPlayer/{idClub}/{idPlayer}", method = RequestMethod.GET)
    public String addPlayerToClub(@PathVariable Long idClub, @PathVariable Long idPlayer, Model model){

        Club club = clubRepository.findById(idClub).get();
        Player player = playerRepository.findById(idPlayer).get();
        player.setClub(club);
        playerRepository.save(player);

        model.addAttribute("club", clubRepository.findById(idClub).get());
        return "club";
    }
}
