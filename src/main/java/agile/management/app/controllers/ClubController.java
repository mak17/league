package agile.management.app.controllers;

import agile.management.app.Services.CoachService;
import agile.management.app.Services.PlayerService;
import agile.management.app.model.Club;
import agile.management.app.model.Player;
import agile.management.app.repositories.ClubRepository;
import agile.management.app.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class ClubController {

    private PlayerRepository playerRepository;
    private ClubRepository clubRepository;

    @Autowired
    PlayerService playerService;
    @Autowired
    CoachService coachService;

    public ClubController(ClubRepository clubRepository, PlayerRepository playerRepository) {
        this.clubRepository = clubRepository;
        this.playerRepository = playerRepository;
    }

    @RequestMapping (value="freePlayers/{id}", method = RequestMethod.GET)
    public String getFreePlayers(@PathVariable Long id, Model model){
        model.addAttribute("club", clubRepository.findById(id).get());
        model.addAttribute("players", playerService.getOnlyFreePlayers());
        return "freePlayers";
    }

     @RequestMapping (value="changeCoach/{clubId}", method = RequestMethod.GET)
     public String showFreeCoaches(@PathVariable Long clubId, Model model){
        model.addAttribute("club", clubRepository.findById(clubId).get());
        model.addAttribute("coaches", coachService.getOnlyFreeChoaches());
        return "freeCoaches";
    }

    @RequestMapping (value="dismiss/{idPlayer}/{idClub}", method = RequestMethod.GET)
    public String dismissPlayer(@PathVariable Long idPlayer, @PathVariable Long idClub, Model model){

        Club club = clubRepository.findById(idClub).get();

        if (club.getPlayers().size() > 1 ) {
            Player player = playerRepository.findById(idPlayer).get();
            club.getPlayers().remove(player);
            clubRepository.save(club);

            player.setClub(null);
            playerRepository.save(player);
        } else {
            model.addAttribute("message", "The club has to has unless one player!!!");
        }
        model.addAttribute("club", clubRepository.findById(idClub).get());
        return "club";
    }
}
