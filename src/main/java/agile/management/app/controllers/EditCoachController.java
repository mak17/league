package agile.management.app.controllers;

import agile.management.app.model.Coach;
import agile.management.app.repositories.CoachRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class EditCoachController {

    private CoachRepository coachRepository;

    public EditCoachController(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @PostMapping("/editCoach/{updateCoachId}")
    public String updateCoach(@PathVariable Long updateCoachId, @ModelAttribute Coach updateCoach, Model model) {
        Coach coach = coachRepository.findById(updateCoachId).get();
        coach.setName(updateCoach.getName());
        coach.setSurname(updateCoach.getSurname());
        coach.setAge(updateCoach.getAge());
        coach.setCountry(updateCoach.getCountry());
        coachRepository.save(coach);
        model.addAttribute("coach", coachRepository.findById(updateCoachId).get());
        return "coachDetails";
    }
}
