package agile.management.app.controllers;

import agile.management.app.model.Coach;
import agile.management.app.repositories.CoachRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class CoachDetailsController {

    CoachRepository coachRepository;

    public CoachDetailsController(CoachRepository coachRepository) {
        this.coachRepository = coachRepository;
    }

    @RequestMapping (value="endCoachCareer/{coachId}", method = RequestMethod.GET)
    public String endCareer(@PathVariable Long coachId, Model model) {
        Coach coach = coachRepository.findById(coachId).get();

        if(coach.getClub() != null) {
            model.addAttribute("message", "At first your club has to dismiss you");
            model.addAttribute("coach", coach);
            return "coachDetails";
        } else {
            coachRepository.delete(coach);
            model.addAttribute("coaches", coachRepository.findAll());
            return "coaches";
        }
    }

    @RequestMapping (value="editFormCoach/{idCoach}", method = RequestMethod.GET)
    public String editFormCoach(@PathVariable Long idCoach, Model model) {
        model.addAttribute("coach", coachRepository.findById(idCoach).get());
        return "editCoach";
    }
}
