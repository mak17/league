package agile.management.app.controllers;

import agile.management.app.model.Club;
import agile.management.app.model.Coach;
import agile.management.app.repositories.ClubRepository;
import agile.management.app.repositories.CoachRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Marcin Ławicki (mak)
 */
@Controller
public class FreeCoachesController {

    private CoachRepository coachRepository;
    private ClubRepository clubRepository;

    public FreeCoachesController(ClubRepository clubRepository, CoachRepository coachRepository) {
        this.clubRepository = clubRepository;
        this.coachRepository = coachRepository;
    }

    @RequestMapping (value="choose/{idClub}/{idCoach}", method = RequestMethod.GET)
    public String addPlayerToClub(@PathVariable Long idClub, @PathVariable Long idCoach, Model model){

        Club club = clubRepository.findById(idClub).get();
        Coach newCoach = coachRepository.findById(idCoach).get();
        Coach oldCoach = coachRepository.findById(club.getCoach().getId()).get();

        club.setCoach(newCoach);
        clubRepository.save(club);

        oldCoach.setClub(null);
        coachRepository.save(oldCoach);

        newCoach.setClub(club);
        coachRepository.save(newCoach);

        model.addAttribute("club", clubRepository.findById(idClub).get());
        return "club";
    }
}
